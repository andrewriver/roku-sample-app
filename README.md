## Roku Sample App ##

### Requirements ###

* Pull data from the server
	* API url: https://ratiointeractive.github.io/Roku-DeveloperDocs/api.json
* The main page should show a RowList with three rows
	* The first row should show the featured items and display the items at 960x540 and centered (like a carousel that does not wrap)
	* The second and third rows should show the recent and bonanza items at 384x216. These rows should each show the row title as well.
* Any item in focus should be entirely visible on the screen
* Clicking on any item from any row should launch a player that plays the associated HLS video.
* The player should support all of the default actions (play/pause/ff/rw)
* Clicking Back from the player should close the player and return to the RowList in its previous state.
* Should work and look correct on FHD/HD/SD resolutions
* Channel and splash logos can be found here:
	* Channel FHD: https://ratiointeractive.github.io/Roku-DeveloperDocs/channel-fhd.png
	* Channel HD: https://ratiointeractive.github.io/Roku-DeveloperDocs/channel-hd.png
	* Channel SD: https://ratiointeractive.github.io/Roku-DeveloperDocs/channel-sd.png
	* Splash FHD: https://ratiointeractive.github.io/Roku-DeveloperDocs/splash-fhd.png
	* Splash HD: https://ratiointeractive.github.io/Roku-DeveloperDocs/splash-hd.png
	* Splash SD: https://ratiointeractive.github.io/Roku-DeveloperDocs/splash-sd.png
	
----
*andres.delrio@zemoga.com | February 2017*