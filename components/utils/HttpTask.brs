'** Http task

sub init()
    ?"-- HttpTask.init"
    m.top.functionName = "doRequest"
end sub

' Main function of the http task. 
' Makes a http get request and stores the response
sub doRequest()
    ?"-- HttpTask.doRequest url = ";m.top.reqUri
    urlTrans = CreateObject("roUrlTransfer")
    urlTrans.SetCertificatesFile("common:/certs/ca-bundle.crt")
    urlTrans.AddHeader("X-Roku-Reserved-Dev-Id", "")
    urlTrans.InitClientCertificates()
    
    urlTrans.setUrl(m.top.reqUri)
    resp = urlTrans.GetToString()
    apiJsonObj = ParseJson(resp)
    
    m.top.resAssocArray = apiJsonObj
end sub