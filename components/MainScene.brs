'** Main Scene handler

sub init()
    m.MainGroup = m.top.FindNode("MainGroup")
    m.MainRowList = m.top.findNode("MainRowList")
    m.VideoPlayer = m.top.findNode("VideoPlayer")
    
    m.MainGroup.setFocus(true)
    m.MainRowList.observeField("rowItemSelected", "onRowItemSelected")
End sub

' Row item selected handler function.
' On select any item on home scene, show Details node and hide Grid.
sub onRowItemSelected()
     ?"> MainScene.onRowItemSelected: ";m.MainRowList.rowItemSelected
     rowItemSelected = m.MainRowList.rowItemSelected
     m.MainGroup.visible = "false"
     setVideo(m.MainRowList.content.getChild(rowItemSelected[0]).getChild(rowItemSelected[1]))
end sub

' Sets the video info with the chosen item
' @param videoInfo content node of the api item data
sub setVideo(videoInfo as Object)
    ?"> MainScene.setVideo : ";videoInfo
    videoContent = createObject("RoSGNode", "ContentNode")
    
    videoContent.url = videoInfo.video
    videoContent.title = videoInfo.title
    videoContent.titleSeason = videoInfo.subtitle
    videoContent.description = videoInfo.description
    videoContent.streamformat = "hls"
    
    m.VideoPlayer.content = videoContent
    m.VideoPlayer.control = "play"
    m.VideoPlayer.setFocus(true)
    m.VideoPlayer.visible = "true"
end sub

' Shows a dialog with the focused item info
sub showDialog()
    focusedContent = m.MainRowList.content.getChild(m.MainRowList.rowItemFocused[0]).getChild(m.MainRowList.rowItemFocused[1])
    dialog = createObject("roSGNode", "Dialog")
    dialog.title = focusedContent.title
    dialog.bulletText = ["Season: " + focusedContent.subtitle,
                        "Description: " + chr(10) + focusedContent.description]
    dialog.optionsDialog = true
    m.top.dialog = dialog
end sub

' Handler of remote control keys
' @param key the remote key
' @param press if key was pressed
' @return true if the event was handled
function onKeyEvent(key as String, press as Boolean) as Boolean
    handled = false
    ?"> MainScene. onKeyEvent. key: ";key;", press: ";press
    if press then
        if key = "back" then
            if m.MainGroup.visible = false and m.VideoPlayer.visible = true then
                m.VideoPlayer.visible = false
                m.MainGroup.visible = true
                m.MainRowList.setFocus(true)
                handled = true
            end if
        else 
            if key = "options" then
                showDialog()
                handled = true
            end if
        end if
    end if
    return handled 
end function
