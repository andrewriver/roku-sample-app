'** Main Group handler

sub init()
    m.MainRowList = m.top.findNode("MainRowList")
    requestData()
    m.MainRowList.setFocus(true)
end sub

' Do the request to api using the HttpTask 
sub requestData()
    m.httpTask = CreateObject("roSGNode", "HttpTask")
    m.httpTask.observeField("resAssocArray", "onHttpTaskResponseChanged")
    m.httpTask.reqUri = "https://ratiointeractive.github.io/Roku-DeveloperDocs/api.json"
    m.httpTask.control = "RUN"
end sub

' Gets the response of the api, parses it and calls method to fill channel content
' @see MainGroup.fillRowListContent
sub onHttpTaskResponseChanged()
    ?"> MainGroup.onHttpTaskResponseChanged response: ";m.httpTask.resAssocArray
    fillRowListContent(m.httpTask.resAssocArray)
end sub 

' Fills the channel content using the api response data
' @param response the parsed api response object
sub fillRowListContent(response as Object)
    m.data = CreateObject("roSGNode", "ContentNode")
    
    'Featured row content
    fillRow(response.featured)
    'Recent row content
    fillRow(response.recent)
    'Bonanza row content
    fillRow(response.bonanza)
    
    m.MainRowList.content = m.data
end sub

' Creates and fills a row of the main RowList
' @param attObj object with the data to fill the row
sub fillRow(attObj as Object)
    row = m.data.CreateChild("ContentNode")
    row.title = attObj.title
    for each item in attObj.items
        rowItem = row.CreateChild("ContentNode")
        rowItem.addFields(item)
    end for
end sub
