'** Main Item handler

sub init()
    m.itemPoster = m.top.findNode("itemPoster") 
    
    m.itemPoster.observeField("loadStatus", "onLoadStatusChanged")
end sub

' Sets image on the row item
sub onItemContentChanged()
    m.itemPoster.uri = m.top.itemContent.image
end sub

' Verifies the status of the poster uri
' If the status is failed, assign a failed image 
sub onLoadStatusChanged()
    if m.itemPoster.loadStatus = "failed" then
        ?"> MainItem.onLoadStatusChanged loadStatus failed for: ";m.itemPoster.uri
        m.itemPoster.uri = "http://margaretlocke.com/wp-content/uploads/2014/01/failed.png"
    end if
end sub