'** Main BrightScript

sub Main()
    print "=== Begins Roku Sample App Channel ==="
    showChannelSGScreen()
end sub

sub showChannelSGScreen()
    'Indicate this is a Roku SceneGraph application
    screen = CreateObject("roSGScreen")
    m.port = CreateObject("roMessagePort")
    screen.setMessagePort(m.port)
    'Create the scene and load the xml component
    scene = screen.CreateScene("MainScene")
    screen.show()

    while(true)
        msg = wait(0, m.port)
        msgType = type(msg)
        if msgType = "roSGScreenEvent"
            if msg.isScreenClosed() then return
        end if
    end while
end sub
